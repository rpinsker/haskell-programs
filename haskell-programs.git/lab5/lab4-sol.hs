module Main where
import Data.Char
import Data.List

main = do 
    putStrLn "Please type an expression:"
    s <- getLine
   -- vars <- getLine
   -- putStrLn (handleExpr s)
    main


--handleExpr s vars = show $ eval (parseExpr s) vars
-- handleExpr s = show $ parseExpr s


data Op = Plus | Mult
    deriving (Show, Eq)

data ArithExpr = Number Integer
                | Operator Op ArithExpr ArithExpr
                | Var String  
    deriving (Show, Eq)


eval :: ArithExpr -> [(String, Integer)] -> Integer
eval (Number n) vars = n
eval (Var str) vars = lookupVar str vars
eval (Operator Plus e1 e2) vars = (eval e1 vars) + (eval e2 vars) -- Evaluate all subexpressions then add
eval (Operator Mult e1 e2) vars = (eval e1 vars) * (eval e2 vars)

lookupVar name variables = case (lookup name variables) of
            Just val -> val -- if the name is in list
            Nothing   -> error ("Unknown name " ++ name)


-- ========================================
-- Simple parsing
-- ------------
simpleParseExpr :: String -> ArithExpr
-- Tim: first we break up into tokens, then convert those into an expression
simpleParseExpr = buildExpr . readTokens

            
simpleReadTokens :: String -> [String]
simpleReadTokens xs = if xs == ""  -- Base case
                    then [] -- no more tokens
                    else token:(simpleReadTokens rest)
                    -- Otherwise, read a single token
                    -- from the start of the string and recurse
    where (token, rest) = simpleReadToken xs


simpleReadToken :: String -> (String, String) -- First string is token
simpleReadToken (x:xs)
    -- Ignore spaces
    | isSpace x = readToken xs
    -- Token is negative number
    | x == '-' && isDigit (head xs) = (x:(takeWhile isDigit xs),
                                            dropWhile isDigit xs)
    -- Token is positive number
    | isDigit x = span isDigit (x:xs)
    -- Token is operator
    | x `elem` ['+','*'] = ([x], xs) -- Token is an operator
    | otherwise = error ("Unrecognised token at head of " ++ (x:xs))

-- buildExpr used for both simple and complex building
buildExpr :: [String] -> ArithExpr
buildExpr ts
    | (length ts) == 1 && isInt (head ts) =  Number (read (head ts))
    | (length ts) == 1 && isLetter (head (head ts)) = Var (head ts)                                         
    -- If it is a single token and not an int or a variable name, the only valid possibility
    -- is that it is a subexpression
    | (length ts) == 1 = parseExpr (head ts)
    -- Plus is checked first because it has lower precedence
    | "+" `elem` ts = Operator Plus (buildExpr plus1) (buildExpr $ tail plus2)
    | "*" `elem` ts = Operator Mult (buildExpr times1) (buildExpr $ tail times2)
    | otherwise = error ("invalid tokens " ++ (show ts))
    where (times1, times2) = break (=="*") ts
          (plus1, plus2) = break (=="+") ts


-- ========================================
-- Differentiation
-- ------------
          
d :: String -> ArithExpr -> ArithExpr         
d _ (Number n) = (Number 0)
d s (Var a) = if s == a then (Number 1)
              else error ("Can't differentiate in terms of a variable not present in expression")                 
d s (Operator Plus e1 e2) = (Operator Plus (d s e1) (d s e2))
d s (Operator Mult e1 e2) = (Operator Plus (Operator Mult e1 (d s e2)) (Operator Mult (d s e1) e2))

-- ========================================
-- Simplification
-- For any expressions not involving variables, simplifies the numbers.Deals with adding zero and multiplying by one and zero. Also deals withif a subexpression has the same expression on two sides of a + to go from a + a to 2*a 
-- ------------
simplify :: ArithExpr -> ArithExpr          
simplify (Operator Plus (Number 0) a) = simplify a           
simplify (Operator Plus a (Number 0)) = simplify a
simplify (Operator Mult a (Number 1)) = simplify a
simplify (Operator Mult (Number 1) a) = simplify a
simplify (Operator Mult a (Number 0)) = (Number 0)
simplify (Operator Mult (Number 0) a) = (Number 0)
simplify (Var a) = (Var a) 
simplify (Number x) = (Number x)
simplify (Operator Plus (Number a) (Number b)) = (Number (a+b)) 
simplify (Operator Plus e1 e2) 
  | e1 == e2 = simplify (Operator Mult (Number 2) e1)
  | otherwise = (Operator Plus (simplify e1) (simplify e2)) 
simplify (Operator Mult (Number a) (Number b)) = (Number (a*b))
simplify (Operator Mult e1 e2) = (Operator Mult (simplify e1) (simplify e2))

                
  

-- ========================================
-- Extra Credit
-- ------------

parseExpr :: String -> ArithExpr
-- Tim: first we break up into tokens, then convert those into an expression
--
--
parseExpr = buildExpr . readTokens
readToken :: String -> (String, String) -- First string is token
readToken (x:xs)
    -- Ignore spaces
    | isSpace x  = readToken xs
    -- Token is negative number
    | x == '-' && isDigit (head xs) = (x:(takeWhile isDigit xs),
                                            dropWhile isDigit xs) 
    -- Token is positive number
    | isDigit x = (takeWhile isDigit (x:xs),
                             dropWhile isDigit xs)
                  
    -- Token is variable              
    | isLetter x = (takeWhile isLetter (x:xs),              
                  dropWhile isLetter xs)
                 
    -- Token is operator
    | x `elem` ['+','*'] = ([x], xs) -- Token is an operator
    | x == '(' = readBracketed (x:xs) -- Token is bracketed subexpression
    | otherwise = error ("Unrecognised token at head of " ++ (x:xs))

readTokens :: String -> [String]
readTokens xs = if xs == ""  -- Base case
                    then [] -- no more tokens
                    else token:(readTokens rest)
                    -- Otherwise, read a single token
                    -- from the start of the string and recurse
    where (token, rest) = readToken xs

isInt :: String -> Bool
isInt (x:xs) = all isDigit xs && ((x == '-') || isDigit x)

-- the first character in the string should be a left
-- parenthesis.  This function reads up until the matching
-- right parenthesis, and returns that string and the remainder
readBracketed :: String -> (String, String)
readBracketed ('(':xs) = findMatching 0 "" xs
    -- the first argument in findMatching keeps track of
    -- how many extra layers of parentheses we are inside
    where -- we found the matching parenthesis as the count is down to zero
          findMatching 0 matched (')':xs) = (matched, xs) 
          -- entering another layer of parentheses
          findMatching n matched ('(':xs) = findMatching (n + 1) (matched ++ "(") xs
          -- exited a layer of parentheses
          findMatching n matched (')':xs) = findMatching (n - 1) (matched ++ ")") xs
          -- non-parenthesis character
          findMatching n matched (x:xs) = findMatching n (matched ++ [x]) xs


test1 = d "x" (Var "x") == Number 1
test2 =  d "x" (Operator Plus (Number 2) (Var "x")) == Operator Plus (Number 0) (Number 1)
test3 = eval (Operator Plus (Var "x") (Var "y")) [("x", 3), ("y", 10)] == 13
test4 =  parseExpr "x + 2" == Operator Plus (Var "x") (Number 2) 
test5 = simplify (Operator Plus (Number 0) (Operator Mult (Number 2) (Number 3))) == (Number 6)
test = test1 && test2 && test3 && test4 && test5 && test6
test6 = simplify (Operator Plus (Operator Mult (Number 1) (Number 5)) (Operator Mult (Operator Plus (Number 2) (Number 0)) (Var "x"))) == (Operator Plus (Number 5) (Operator Mult (Number 2) (Var "x")))