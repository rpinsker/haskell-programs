data Op = Plus | Mult
                 deriving (Show, Eq)

data ArithExpr = Number Integer 
                 | Operator Op ArithExpr ArithExpr
                   deriving (Show, Eq)

testExpr1 = Operator Plus (Operator Plus (Number 1) (Number 2)) (Number 3)
testExpr2 = Operator Plus (Operator Mult (Number 4) (Number 2)) (Number 3)
testExpr3 = (Operator Plus (Operator Plus (Number 1) (Operator Mult (Number 2) (Number 3))) (Operator Mult (Operator Mult (Number -5) (Number 3)) (Operator Plus (Number 4) (Number -2))))

