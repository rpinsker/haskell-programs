import System.Environment
import Data.List

{-
Finds the k most frequent terms from given N terms.

Input format: first line of input contains N, denoting the number of terms user will input on the following lines (1 word per N lines).

Next line contains k, denoting what number of the most frequent terms should be printed.

Output format: the k most frequent terms in descending order of their frequency. 
-}
main = do 
     n <- getLine
     let nInt = (read n) :: Int
     args <- getLines nInt
     k <- getLine
     let kInt = (read k) :: Int
     let xs = lines args
     let sortedXS = sort xs
     let stringsAndAppearances = zip (nub sortedXS) (filterBy sortedXS) :: [(String, Int)]
     let output = take kInt (orderAppearances stringsAndAppearances)
     putStr $ unlines $ output

--getLines is called from the main function and works to recursively create an IO String (or IO [Char]) 
--that has each Int input entered seperated by new lines. It uses a counter (n, or the first line of 
--input from the user) to determine how many lines of Ints it should read.
getLines :: Int -> IO [Char]
--getLines 0 = return ""
getLines 1 = do
         x <- getLine
         return x
getLines n = do
         x <- getLine
         ys <- getLines (n-1)
         return $ x ++ "\n" ++ ys

--filterBy counts how many times the first word in the sorted list of inputs appears and then makes that the first element of a new output list. It recursively works to apply itself to the original list without the first element which has already been counted. This is attached as the tail of the output list.
filterBy :: [String] -> [Int]
filterBy (x:xs) = (length (filter (== x) xs) + 1) : filterBy (filter (/= x) xs)
filterBy [] = []

--orderAppearances finds the index of the element in which the string appears the most. It then creates an output list in which the the string of highest appearance is the head. It then recursively works to apply itself to the original list without the element of highest appearance. This list is attached as the tail of the output list. 
orderAppearances :: [(String,Int)] -> [String]
orderAppearances [] = [] 
orderAppearances xs = case (elemIndex (maximum (map snd xs)) (map snd xs)) of
                    Nothing -> []
                    Just a -> fst (xs !! a) : orderAppearances (delete (xs !! a) xs)