import MancalaBoard

main :: IO ()
main = do
  putStrLn ("Welcome to Mancala")
  putStrLn (" RULES ")
  putStrLn ("To quit, type -1 instead of a pit to move to. \n")
  playGame initial



playGame :: MancalaBoard -> IO ()
playGame board = do
  putStrLn (show board)
  putStrLn ("\n" ++ (show (getCurPlayer board)) ++ ", please enter the pit number you would like to move stones from. Allowed moves include: " ++ (show (allowedMoves board (getCurPlayer board)))) 
  args <- getLine
  let x = read args
  case x of      
    (-1) -> putStrLn ("Quit.")
    _ -> case (isAllowedMove x board) of
      True -> do
        let newBoard = move board x
       -- putStrLn (show newBoard)            
        case (gameOver newBoard) of
          True -> endGame newBoard
          False -> playGame newBoard
      False -> do 
        putStrLn ("Please try again with a valid move.")
        playGame board  
  
  
endGame :: MancalaBoard -> IO ()
endGame board = do
  let xs = winners board
  case xs of    
    [] -> putStrLn ("The game is a draw.")
    x:_ -> putStrLn ((show x) ++ " is the winner.")